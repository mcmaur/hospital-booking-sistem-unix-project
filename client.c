#include "header_proj.h"

int main(int argc,char * argv[])
{
  /*dichiaro variabili*/
  int num_figli,num_servizio,livello_priorita,i,main_bus_id,padre,ctrl;

  /*assegnazione variabili o da linea di comando argv oppure predefinte*/
  if(argc==4)
  {
    num_figli=atoi (argv[1]);

    if(strcmp(argv[2],"OPHTHALMOLOGY")==0)
      num_servizio=0;
    else
      if(strcmp(argv[2],"ORTHOPEDICS")==0)
	num_servizio=1;
      else
	if(strcmp(argv[2],"RADIOLOGY")==0)
	  num_servizio=2;
	else
	{
	  fprintf(stderr,"Input %s non riconosciuto",argv[2]);
	  exit(1);
	}

    if(strcmp(argv[3],"ILLWAIT")==0)
      livello_priorita=0;
    else
      if(strcmp(argv[3],"NOHURRY")==0)
	livello_priorita=1;
      else
	if(strcmp(argv[3],"IMONHOLODAY")==0)
	  livello_priorita=2;
	else
	  if(strcmp(argv[3],"NEEDTOGO")==0)
	    livello_priorita=3;
	  else
	    if(strcmp(argv[3],"CANTWAIT")==0)
	      livello_priorita=4;
	    else
	    {
	      fprintf(stderr,"Input %s non riconosciuto\n",argv[3]);
	      exit(1);
	    }
  }
  else
  {
    num_figli=0;
    num_servizio=RADIOLOGY;
    livello_priorita=ILLWAIT;
  }

  /*aggancio a coda create da server*/
  main_bus_id=msgget(SERVER_KEY,0);
  if(main_bus_id==-1)
  {
    perror("Mancato aggancio al main_bus:");
    exit(1);
  }
  #if defined(DEBUG)
    fprintf(stderr,"Aggancio al main_bus riuscito con id %d\n",main_bus_id);
  #endif

  /*fork per num_figli figli*/
  for(i=0;i<num_figli;i++)
  {
    padre=fork();
    if(!padre) i=num_figli;
  }

  /*invio richiesta su bus principale di comunicazione con il server*/
  if(padre)
  {
    for(i=1;i<num_figli;i++)
      wait(NULL);
    #if defined(DEBUG)
      fprintf(stderr,"Figli completato ordine: chiudo\n");
    #endif
  }
  else
  {
    /*figli:dichiarazione variabili*/
    request request;
    response response;

    /*figli:richiesta coda server*/
    request.mtype=FOR_SERVER;
    request.clientId=getpid();
    request.priority=livello_priorita;
    request.kindof_service=num_servizio;
    /*-------------------------------------------------------------------------------------------------------------------------*/
    #if defined(DEBUG)
      if (DEBUG) printf("----------------DEBUG   request.mtype:%d; request.clientId:%d; clientId(from pid):%d; request.priority:%d; request.kindof_service:%d;\n",request.mtype,request.clientId,getpid(),request.priority,request.kindof_service);
    #endif
    /*---------------------------------------------------------------------------------------------------------------------------*/

    /*figli:invio la richiesta al server attraverso il main_bus*/
    ctrl=msgsnd(main_bus_id,&request,sizeof(request)-sizeof(long),0);
    if(ctrl==-1)
    {
      perror("Mancato invio messaggio su coda main");
      exit(1);
    }
    #if defined(DEBUG)
      fprintf(stderr,"Invio messaggio riuscito\n");
    #endif

    /*figli:devo ricevere la risposta*/
    ctrl=msgrcv(main_bus_id,&response,sizeof(response)/*-sizeof(long)*/,getpid(),0);
    if(ctrl==-1)
    {
      perror("Mancato ricezione risposta su coda main");
      exit(1);
    }
    #if defined(DEBUG)
      fprintf(stderr,"Ricezione risposta riuscita\n");
    #endif

    /*figli:stampo a video il turno e il costo*/
    printf("Il turno associato alla prenotazione richiesta è: %d\n",request.turn);
    printf("Il costo associato alla prenotazione richiesta è: %d\n",request.price);

    exit(0);
  }

}