all:
		gcc -Wall -pedantic attuatore.c -o attuatore.o
		gcc -Wall -pedantic server.c -o server.o
		gcc -Wall -pedantic client.c -o client.o
	
attuatore.c:	header_proj.h server.o
		gcc -Wall -pedantic attuatore.c -o attuatore.o

server.c:	header_proj.h
		gcc -Wall -pedantic server.c -o server.o

client.c:	header_proj.h server.o
		gcc -Wall -pedantic client.c -o client.o
clean:
	rm -rf *.o