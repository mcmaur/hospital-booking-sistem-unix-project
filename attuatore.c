#include "header_proj.h"

int oph_id,rad_id,ort_id;

void aggancio()
{
  /*aggancio shared memory			?????*/

  /*aggancio code: oculistica*/
  oph_id=msgget(OPH_queue_KEY,PERMESSI);
  if(oph_id==-1)
  {
    perror("Mancata aggancio del OPH_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Aggancio del OPH_bus riuscito con id %d\n",oph_id);
    #endif
  }

  /*aggancio code: radiologia*/
  rad_id=msgget(RAD_queue_KEY, PERMESSI);
  if(rad_id==-1)
  {
    perror("Mancata aggancio del rad_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Aggancio del rad_bus riuscito con id %d\n",rad_id);
    #endif
  }

  /*aggancio code: ortopedia*/
  ort_id=msgget(ORT_queue_KEY, PERMESSI);
  if(ort_id==-1)
  {
    perror("Mancata aggancio al ort_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Aggancio del ort_bus riuscito con id %d\n",ort_id);
    #endif
  }
  
}

char * make_stringREP(int a)
{
  char *string=(char*)malloc(sizeof(char));
  if(a == 0)
	strcpy(string, "Oculistica");
  if(a == 1)
	strcpy(string, "Ortopedia");
  if(a == 2)
	strcpy(string, "Radiologia");
  return string;
}

char * make_stringPRIOR(int a)
{
  char *string=(char*)malloc(sizeof(char));
  if(a == 0)
	strcpy(string, "I'll wait");
  if(a == 1)
	strcpy(string, "No hurry");
  if(a == 2)
	strcpy(string, "I'm on holiday");
  if(a == 3)
	strcpy(string, "Need to go");
  if(a == 4)
	strcpy(string, "Can't wait");
  return string;
}

int main(int argc, char **argv){
	
	int shm_id;
	int *counter;
	int i, inputPid, tempID, flag, stop=1, scelta;
	int radInd = 0, ophInd = 0, ortInd = 0;
	reservation reser;
	FILE *log;
	
	counter = (int *)shmat(shm_id,NULL,0);
	reservation sortArray [*counter+1];
	
	reservation ophQ [*counter+1];
	reservation radQ [*counter+1];
	reservation ortQ [*counter+1];
	
	for(i=1; i<=*counter; i++){
		flag=1;
		while(flag){
			if(msgrcv(rad_id, &reser, sizeof(reservation)-sizeof(long), i, IPC_NOWAIT) != -1){
				radQ[radInd].clientId = reser.clientId;
				radQ[radInd].price = reser.price;
				radQ[radInd].kindof_service = reser.kindof_service;
				radQ[radInd].priority = reser.priority;
				radInd++;
			}
			else flag=0;
		}
		flag=1;
		while(flag){
			if(msgrcv(oph_id, &reser, sizeof(reservation)-sizeof(long), i, IPC_NOWAIT) != -1){
				ophQ[ophInd].clientId = reser.clientId;
				ophQ[ophInd].price = reser.price;
				ophQ[ophInd].kindof_service = reser.kindof_service;
				ophQ[ophInd].priority = reser.priority;
				ophInd++;
			}
			else flag=0;
		}
		flag=1;
		while(flag){
			if(msgrcv(ort_id, &reser, sizeof(reservation)-sizeof(long), i, IPC_NOWAIT) != -1){
				ortQ[ortInd].clientId = reser.clientId;
				ortQ[ortInd].price = reser.price;
				ortQ[ortInd].kindof_service = reser.kindof_service;
				ortQ[ortInd].priority = reser.priority;
				ortInd++;
			}
			else flag=0;
		}
	}
	
	while(stop){
		printf("\n\n");
		printf("\n******************************	MENU	*****************************");
		printf("\n																	    ");
		printf("\n	1  - Stampa l'ordine di prenotazione dei reparti					");
		printf("\n  2  - Ricerca e visualizza la prenotazione di un determinato PID		");
		printf("\n 	3  - Ordina e stampa la lista secondo il numero del cliente			");
		printf("\n  4  - Stampa il log del sistema di prenotazione						");
		printf("\n  5  - Esci															");
		printf("\n																		");
		printf("\n***********************************************************************");
		printf("\n\nFai la tua scelta: ");
				
		scanf("%d", &scelta);
		switch(scelta){
			case 1: printf("\n\n\t\t Prenotazioni per il reparto di RADIOLOGIA:\n\n");
					for(i=0; i<radInd; i++)
						printf("Turno: %d\tCliente: %d\t\tPriorita': %s\n", i, radQ[i].clientId, (char *)make_stringPRIOR(radQ[i].priority));
					printf("\n=================================================================\n");
					
					
					printf("\n\n\t\t Prenotazioni per il reparto di OCULISTICA:\n\n");
					for(i=0; i<ophInd; i++)
						printf("Turno: %d\tCliente: %d\t\tPriorita': %s\n", i, ophQ[i].clientId, (char *)make_stringPRIOR(ophQ[i].priority));
					printf("\n=================================================================\n");
					
					
					printf("\n\n\t\t Prenotazioni per il reparto di ORTOPEDIA:\n\n");
					for(i=0; i<ortInd; i++)
						printf("Turno: %d\tCliente: %d\t\tPriorita': %s\n", i, ortQ[i].clientId, (char *)make_stringPRIOR(ortQ[i].priority));
					printf("\n=================================================================");
					
					system("clear");
					printf("\n\nPer vedere la lista completa, sali nel terminale\n\n");
					
					break;
					
					
			case 2: system("clear");
					flag=0;
					printf("\nInserire il PID del processo: ");
					scanf("%d", &inputPid);
					for(i=0; i<radInd; i++){
						if(radQ[i].clientId == inputPid){
							printf("\nReparto: %s\tTurno: %d\tPriorita': %s\tImporto: %d€",(char *)make_stringREP(radQ[i].kindof_service), i, (char *)make_stringPRIOR(radQ[i].priority), reser.price);
							flag=1;
						}
					}
					for(i=0;i<ophInd;i++){
						if(ophQ[i].clientId == inputPid){
							printf("\nReparto: %s\t Turno: %d\t Priorita': %s\tImporto: %d€",(char *)make_stringREP(ophQ[i].kindof_service), i, (char *)make_stringPRIOR(ophQ[i].priority), reser.price);
							flag=1;
						}
					}
					
					for(i=0;i<ortInd;i++){
						if(ortQ[i].clientId == inputPid){
							printf("\nReparto: %s\t Turno: %d\t Priorita': %s\tImporto: %d€",(char *)make_stringREP(ortQ[i].kindof_service), i, (char *)make_stringPRIOR(ortQ[i].priority), reser.price);
							flag=1;
						}
					}
					if(flag==0) printf("\n\nIl PID %d non ha eseguito alcuna richiesta\n\n", inputPid);
					break;
					
			case 3: system("clear");
					/*qsort(ophQ, ophInd, sizeof(reservation), cmp);	si lamenta il compilatore in cmp
					qsort(ortQ, ortInd, sizeof(reservation), cmp);
					qsort(radQ, radInd, sizeof(reservation), cmp);*/
			
					printf("\n\n\t\t Prenotazioni per il reparto di Radiologia:\n");
					for(i=0;i<radInd;i++)
						printf("Turno: %d\t Cliente: %d\t Priorita: %s\n",i,radQ[i].clientId, (char *)make_stringPRIOR(radQ[i].priority));
					printf("\n=================================================\n");
					
					printf("\n\n\t\t Prenotazioni per il reparto di Oculistica:\n");
					for(i=0;i<ophInd;i++)
						printf("Turno: %d\t Cliente: %d\t Priorita: %s\n",i,ophQ[i].clientId, (char *)make_stringPRIOR(ophQ[i].priority));
					printf("\n=================================================\n");
					
					printf("\n\n\t\t Prenotazioni per il reparto di Ortopedia:\n");
					for(i=0;i<ortInd;i++)
						printf("Turno: %d\t Cliente: %d\t Priorita: %s\n",i,ortQ[i].clientId, (char *)make_stringPRIOR(ortQ[i].priority));
					printf("\n=================================================\n");
					break;

					
			case 4:	log=fopen("log.txt", "rw");		//SEG FAULT se non c'è il file ma cmq non ci scrive dentro pur senza dare errore
					
					system("clear");		
					fprintf(log, "**************************	OFTALMOLOGIA	*************************************\n");
					for(i=0; i<ophInd; i++){
						fprintf(log, "Turno: %d\tCliente: %d\tPriorita: %s\n", i, ophQ[i].clientId, (char *)make_stringPRIOR(ophQ[i].priority));
						printf("\naaa");
					}
					fprintf(log,"\n=================================================\n");
						
							
					fprintf(log, "\n***************************	RADIOLOGIA	**************************************\n");
					for(i=0;i<radInd;i++)
						fprintf(log, "Turno: %d\tCliente: %d\tPriorita: %s\n", i, radQ[i].clientId, (char *)make_stringPRIOR(radQ[i].priority));
					fprintf(log,"\n=================================================\n");
					
							
					fprintf(log, "\n****************************	ORTOPEDIA	***************************************\n");
					for(i=0;i<ortInd;i++)
						fprintf(log, "Turno: %d\tCliente: %d\tPriorita: %s\n", i, ortQ[i].clientId, (char *)make_stringPRIOR(ortQ[i].priority));
					fprintf(log,"\n=================================================\n");
					
					break;
					
			case 5:	stop=0;
							printf("\nProgramma terminato\n\n");
							break;
			default: printf("\n%d - Comando non valido\n", scelta);
		}
	}
	
	return 0;

}
