#include "header_proj.h"
int main_bus_id,oph_id,rad_id,ort_id,sem_pool,shm_id;

/*---------------------------------------------------------handler per SIGQUIT-----------------------------------------------------------------------------------------------------*/
void handler_quit (int signum)
{
  int ctrl;
  printf("\n\nAttendere prego...\nDeallocazione code e semafori in corso prego\n");
  ctrl=msgctl(main_bus_id,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione main coda");
  ctrl=msgctl(oph_id,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione oph coda");
  ctrl=msgctl(rad_id,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione rad coda");
  ctrl=msgctl(ort_id,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione ort coda");
  ctrl=semctl(sem_pool,0,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione semaforo");
  ctrl=shmctl(shm_id,IPC_RMID,0);
  if(ctrl==-1)
    perror("\tErrore deallocazione memoria condivisa");
  printf("Deallocazione completata...\nGrazie per aver usato il nostro programma\n");
  exit(0);
}

/*--------------------------------------------allocazione strutture necessarie-----------------------------------------------------------------------------------------------------*/
void allocazione()
{
  int ctrl;
  
  /*alloco semaforo per mutua esclusione di scrittura/lettura su sequenza*/
  sem_pool=semget(IPC_PRIVATE,1, PERMESSI);
  if(sem_pool==-1)
  {
    perror("Mancata allocazione semafori: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione pool semafori riuscita con id %d\n",sem_pool);
    #endif
  }

  /*assegnamento valore a semaforo*/
  semctl(sem_pool,0,SETVAL,1);

  /*allocazione code: main bus*/
  main_bus_id=msgget(SERVER_KEY,IPC_CREAT | PERMESSI);
  if(main_bus_id==-1)
  {
    perror("Mancato allocazione del main_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione main_bus riuscito con id %d\n",main_bus_id);
    #endif
  }

  /*allocazione code: oculistica*/
  oph_id=msgget(OPH_queue_KEY,IPC_CREAT | PERMESSI);
  if(oph_id==-1)
  {
    perror("Mancata allocazione del OPH_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione del OPH_bus riuscito con id %d\n",oph_id);
    #endif
  }

  /*allocazione code: radiologia*/
  rad_id=msgget(RAD_queue_KEY,IPC_CREAT | PERMESSI);
  if(rad_id==-1)
  {
    perror("Mancata allocazione del rad_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione del rad_bus riuscito con id %d\n",rad_id);
    #endif
  }

  /*allocazione code: ortopedia*/
  ort_id=msgget(ORT_queue_KEY,IPC_CREAT | PERMESSI);
  if(ort_id==-1)
  {
    perror("Mancata allocazione al ort_bus: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione del ort_bus riuscito con id %d\n",ort_id);
    #endif
  }

  /*allocazione shared memory*/
  shm_id=shmget(SHM_KEY,sizeof(int),IPC_CREAT | PERMESSI);
  if(shm_id==-1)
  {
    perror("Mancata allocazione della memoria condivisa: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Allocazione della memoria riuscita con id %d\n",shm_id);
    #endif
  }

  /*verifico esistenza ed eventualmente creo cartella delle fatture*/
  ctrl=mkdir("./"INVOICE_PATH,0755);
  if(ctrl==-1 && errno!=EEXIST)
  {
	perror("Mancata creazione cartella fatture: ");
	exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Creazione della cartella delle fatture riuscita oppure cartella già presente\n");
    #endif
  }
}

/*------------------------------------------------gestione file configurazione-----------------------------------------------------------------------------------------------------*/
int readNextInt(char *stringa)
{
  int i=0, attiva_lettura=0, k=0;
  char costo [4];
  while(stringa[i]!=';')
  {
    if(stringa[i]==' ')
	{
      attiva_lettura=1;
      i++;
    }
    if(attiva_lettura==1)
	{
      costo[k]=stringa[i];
      k++;
    }
    i++;
  }
  return atoi(costo);
}

int lettura_config ( char * parola_da_cercare)
{
  char str[30] = {0};
  FILE *fp;
  #if defined(DEBUG)
	fprintf(stderr,"Parola da cercare: %s\n",parola_da_cercare);
  #endif
  /*apertura file configurazione*/
  fp=fopen("./config.conf","r");
  if(fp==NULL)
  {
    perror("Errore nell'apertura del file di configurazione: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Apertura file configurazione avvenuta correttamente\n");
    #endif
  }
  /*char *fgets(char *s, int size, FILE *stream);*/
  while(!feof(fp))
  {
    fgets(str,30,fp);
    if(str[0] != '#')
    {
      if (strstr(str,parola_da_cercare))
        return readNextInt(str);
    }
  }
  return -1;
}

/*------------------------------------------------operazioni su semaforo: semop-----------------------------------------------------------------------------------------------------*/
void neg_semop()
{
    struct sembuf sops;
    sops.sem_num=0;
    sops.sem_op=-1;
    sops.sem_flg=0;
    semop(sem_pool,&sops,1);
}

void pos_semop()
{
  struct sembuf sops;
  sops.sem_num=0;
  sops.sem_op=+1;
  sops.sem_flg=0;
  semop(sem_pool,&sops,1);
}

/*------------------------------------------------------------------------MAIN-----------------------------------------------------------------------------------------------------*/
int main(int argc,char ** argv)
{
  /*dichiarazione variabili interne*/
  request request;
  int padre,ctrl,costo_radiologica,costo_oculistica,costo_ortopedia,costo_priorita;
  int *sequenza;
  char *tipo_servizio;

  /*dichiaro variabili per data/ora*/
  time_t rawtime;
  struct tm* leggibile;
  time (&rawtime);

  /*ricerca in file config*/
  costo_radiologica=lettura_config("VISITA_RADIOLOGICA");
  costo_oculistica=lettura_config("VISITA_OCULISTICA");
  costo_ortopedia=lettura_config("VISITA_ORTOPEDICA");
  costo_priorita=lettura_config("COSTO_PRIORITA");
  
  #if defined(DEBUG)
    fprintf(stderr,"costo_radiologica: %d\n",costo_radiologica);
    fprintf(stderr,"costo_oculistica: %d\n",costo_oculistica);
    fprintf(stderr,"costo_ortopedia: %d\n",costo_ortopedia);
    fprintf(stderr,"costo_priorita: %d\n",costo_priorita);
  #endif

  /*richiamo il metodo per allocare le code e la memoria condivisa*/
  allocazione();

  /*attach alla memoria condivisa
  void *shmat(int shmid, const void *shmaddr, int shmflg);*/
  sequenza=shmat(shm_id,0,0);
  if(sequenza==(void*)-1)
  {
    perror("Errore nell'attach alla memoria condivisa: ");
    exit(1);
  }
  else
  {
    #if defined(DEBUG)
      fprintf(stderr,"Attach alla memoria condivisa avvenuta correttamente\n");
    #endif
  }
  (*sequenza) = 4;

  /*associazione dell'handler per deallocare tutto al segnale SIQUIT*/
  signal(SIGQUIT,handler_quit);

  printf("In attesa di richieste...");
  
  for(;;)
  {
	/*printf("In attesa di richieste...");*/
	
    /*ricezione richiesta coda server*/
    ctrl=msgrcv(main_bus_id,&request,sizeof(request)/*-sizeof(long)*/,FOR_SERVER,0);
    if(ctrl==-1)
    {
      perror("Errore comunicazione su main_bus: ");
      exit(1);
    }
    else
    {
      #if defined(DEBUG)
        fprintf(stderr,"Ricezione msg su main_bus avvenuta\n");
      #endif
    }

    /*cattura del tempo*/
	leggibile = localtime(&rawtime);

    /*------------------------------------------------------pure debugging-------------------------------------------------------*/
	#if defined(DEBUG)
	  fprintf(stderr,"----------------DEBUG   request.mtype:%d; request.clientId:%d; request.priority:%d; request.kindof_service:%d;\n",request.mtype,request.clientId,request.priority,request.kindof_service);
	#endif
	/*----------------------------------------------------------------pure debugging---------------------------------------------*/

    /*fork all'arrivo della richiesta*/
    padre=fork();

    /*---------------------------------------------------------------pure debugging---------------------------------------------*/
    #if defined(DEBUG)
      fprintf(stderr,"---------fork effettuata: padre: %d--------------\n", padre);
    #endif
    /*-------------------------------------------------------------------pure debugging-----------------------------------------*/

    if(padre)
    {
      /*padre: attesa infinita
      //-------------------------------------------------------------------------------------------------------------------------*/
      #if defined(DEBUG)
        fprintf(stderr,"---------parte del programma del padre--------------\n");
      #endif
      /*-------------------------------------------------------------------------------------------------------------------------*/
    }
    else
    {
      /*figli: dichiarazione variabili*/
      int costo_servizio;
      /*reservation reservation;*/
	  FILE *fattura_file;
      char fattura_file_nome[FILENAMELEN];

      /*figli: attach alla memoria condivisa*/
      int *sequenza=shmat(shm_id,0,0);
      #if defined(DEBUG)
        fprintf(stderr,"---------attach shared memory per sequenza effettuato--------------\n");
      #endif
      /*--------------------------------------------------------------------------------------------------------------------------*/

      /*figli: calcolo numero turno:sequenza - numero priorità in mutua esclusione su semaforo 0*/
      neg_semop();
      request.turn=(*sequenza)-request.priority;
      (*sequenza)++;
      pos_semop();
      #if defined(DEBUG)
        fprintf(stderr,"---------calcolo sequenza effettuato: %d--------------\n",request.turn);
      #endif
      /*---------------------------------------------------------------------------------------------------------------------------*/

      /*figli: calcolo costo prestazione: costo_visita +(priorità * costo_priorità)*/
      if(request.kindof_service==2)
      {
        tipo_servizio="VISITA_RADIOLOGICA";
        costo_servizio=costo_radiologica;
      }
      else if(request.kindof_service==0)
      {
        tipo_servizio="VISITA_OCULISTICA";
        costo_servizio=costo_oculistica;
      }
      else if(request.kindof_service==1)
      {
        tipo_servizio="VISITA_ORTOPEDICA";
        costo_servizio=costo_ortopedia;
      }
      /*figli:request price of indicated service*/
      request.price=costo_servizio+(request.priority*costo_priorita);

      /*---------------------------------------------------------------------------------------------------------------------------*/
      #if defined(DEBUG)
        fprintf(stderr,"---------calcolo della prestazione: %d--------------\n",request.price);
      #endif
      /*---------------------------------------------------------------------------------------------------------------------------*/

      /*figli: setto mtype con PID del richiedente per permettere al client di riconoscere le risposte alle sue richieste*/
      request.mtype=request.clientId;

       /*figli:"trasformo" request in reservation (semplice cambio di nome)*/
      /*reservation=request;*/																			/*ERRORE!!!!!!!!!!!!!!*/

	  /*figli: invio prenotazione a code specifiche*/
      if(request.kindof_service==2)/*VISITA_RADIOLOGICA*/
      {
        ctrl=msgsnd(rad_id,&request,sizeof(request)-sizeof(long),0);
		if(ctrl==-1)
		{
		  perror("Mancato invio prenotazione coda radiologia");
		  exit(1);
		}
		else
		  fprintf(stderr,"Invio prenotazione radiologia riuscito\n");
		
      }
      else if(request.kindof_service==0)/*VISITA_OCULISTICA*/
      {
        ctrl=msgsnd(oph_id,&request,sizeof(request)-sizeof(long),0);
		if(ctrl==-1)
		{
		  perror("Mancato invio prenotazione su coda oculistica");
		  exit(1);
		}
		else
		  fprintf(stderr,"Invio prenotazione coda oculistica riuscito\n");
      }
      else if(request.kindof_service==1)/*VISITA_ORTOPEDICA*/
      {
        ctrl=msgsnd(ort_id,&request,sizeof(request)-sizeof(long),0);
		if(ctrl==-1)
		{
		  perror("Mancato invio prenotazione su coda ortopedia");
		  exit(1);
		}
		else
		  fprintf(stderr,"Invio prenotazione coda ortopedia riuscito\n");
      }
	  
      /*figli: scrivo fattura*/
      ctrl=sprintf(fattura_file_nome,"./"INVOICE_PATH"%d.txt",request.clientId);
	  if(ctrl==-1)
      {
        perror("Errore nella sprintf: ");
        exit(1);
      }
      else
      {
        #if defined(DEBUG)
          fprintf(stderr,"Sprintf avvenuta correttamente\n");
        #endif
      }

      /*assegno a receipt_fname il nome della del file contenente la fattura*/
	  /*request.receipt_fname=request.clientId".txt";												ERRORE!!!!!!!!!!!!!!*/

	  #if defined(DEBUG)
	  printf("fattura_file_nome:%s\n",fattura_file_nome);
	  #endif

	  fattura_file=fopen(fattura_file_nome,"w+");
      if(fattura_file==NULL)
      {
        perror("Errore nell'apertura del file di fattura: ");
        exit(1);
      }
      else
      {
        #if defined(DEBUG)
          fprintf(stderr,"Apertura file di fattura avvenuta correttamente\n");
        #endif
      }
      fprintf(fattura_file,"Destinatario: %d\nData: %d/%d/%d\nora: %d:%d\nServizio richiesto: %s\nTurno: %d\nCosto: %d",request.clientId,leggibile->tm_mday,leggibile->tm_mon,leggibile->tm_yday+1785,leggibile->tm_hour,leggibile->tm_min,tipo_servizio,request.turn,request.price);
      fclose(fattura_file);

	  /*invio la risposta dell'avvenuta prenotazione al client*/
	  ctrl=msgsnd(main_bus_id,&request,sizeof(request)-sizeof(long),0);
	  if(ctrl==-1)
	  {
		perror("Mancato invio risposta su coda main");
		exit(1);
	  }
	  #if defined(DEBUG)
		fprintf(stderr,"Invio risposta riuscito\n");
	  #endif

      /*figli: distacco da memoria condivisa*/
      shmdt(sequenza);

      /*figli: terminazione*/
      exit(0);
    }
  }
}